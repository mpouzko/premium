// @koala-prepend "jquery-2.2.4.js";
// @koala-prepend "scrolloverflow.min.js";
// @koala-prepend "jquery.fullpage.js";
// @koala-prepend "jquery.magnific-popup.min.js";

(function($) {
	$(document).ready(function() {
		$('#fullpage').fullpage( {
			scrollOverflow:true,
			anchors:['home', 'service', 'tariff', 'business','executive', 'van', 'autos', 'contact' ],
			menu: '#menu',
	   		responsiveWidth: 200,
			afterLoad: function(anchorLink, index){
				if (index>1) {
					$(".call-btn").show();
				}
				else {
					$(".call-btn").hide();	
				}
				if(index >= 3 && index <= 6){
					$("#menu li:eq(2)").addClass("active");
				}
				else {
					$("#menu li:eq(2)").removeClass("active");
				}
			}
		});
		$(".go-down").click(function(event) {
			$.fn.fullpage.moveSectionDown();
		});
		$(".scroll-up").click(function(event) {
			$.fn.fullpage.moveSectionUp();
		});
		$(".scroll-down").click(function(event) {
			$.fn.fullpage.moveSectionDown();
		});

		$(".form").submit(function(event) {
			event.preventDefault();
    		$(this).find("button").attr("disabled",true).text("отправка...");
    		var form = this;
    		
    		$.ajax({
    		  method: "POST",
    		  url: "send.php",
    		  data: $(form).serialize()
    		})
    		  .done(function( msg ) {
    		  	if (msg == "OK" ) {
	    		    alert( "Спасибо за обращение! Мы свяжемся с вами в ближайшее время.");
	    			$(form).find("button").text("Сообщение отправлено");	    
    			}
    			else {
    				$(form).find("button").attr("disabled",false).text("Отправить");	    
    		    	alert( msg );
    		    }
    		  })
    		  .error(function( msg ) {
    		    	
    		    	$(form).find("button").attr("disabled",false).text("Отправить");	    
    		    	alert( msg );
    		  });
				
		});


		$("li.hint").click(function(event) {
			$(this).children(".hide").removeClass("hide");
		});

	});



	$(window).load(function() {
		$(".popup").magnificPopup({
			  type: 'image'
			  
			});
	});

})(jQuery)
